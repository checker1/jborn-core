package ru.maksaimer.s09;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadCSVFileExample {
    static class User {
        private Integer id;
        private String firstName;
        private String lastName;

        public User(Integer id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("/home/maksaimer/jborn/jborn-core/src/ru/maksaimer/s09/example.csv");
        InputStreamReader reader = new InputStreamReader(fis, "UTF-8");
        BufferedReader bf = new BufferedReader(reader);

        String s = bf.readLine();
        String[] columns = s.split(";");
        System.out.println(columns);

        while (true) {
            s = bf.readLine();
            if (s == null) {
                break;
            }

            String[] data = s.split(";");

            System.out.println(new User(Integer.valueOf(data[0]), data[1], data[2]));
        }
    }
}
