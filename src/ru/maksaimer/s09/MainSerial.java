package ru.maksaimer.s09;

import java.io.*;

public class MainSerial {
    static class User implements Serializable {
        private final int version = 2;

        private String firstName;
        private String lastName;

        public User(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "User{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) throws Exception {
        String filePath = "/home/maksaimer/jborn/jborn-core/src/ru/maksaimer/s09/user.byte";

        User user = new User("Mikhail", "Maksaimer");
        write(filePath, user);

        User userFormFS = read(filePath);
        System.out.println(userFormFS);
        System.out.println(userFormFS == user);
    }

    public static void write(String path, Serializable object) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
    }

    public static <T> T read(String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return (T) ois.readObject();
    }
}
