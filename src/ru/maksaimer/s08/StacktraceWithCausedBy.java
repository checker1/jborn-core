package ru.maksaimer.s08;

import java.io.IOException;

public class StacktraceWithCausedBy {
    public static void main(String[] args) {
        sendIfEven(2, "Hello message broker!");
    }

    private static void sendIfEven(int i, String message) {
        if (i % 2 == 0) {
            sendMessage(message);
        }
    }

    private static void sendMessage(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message can not be null!");
        }

        try {
            sendBinaryMessage(message);
        } catch (IOException e) {
            throw new IllegalStateException("Problems message sending", e);
        }
    }

    private static void sendBinaryMessage(String message) throws IOException {
        // здесь допустим произошел сбой при подключеии к очереди сообщений
        throw new IOException("Bad message");
    }

}
