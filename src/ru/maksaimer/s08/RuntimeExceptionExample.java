package ru.maksaimer.s08;

public class RuntimeExceptionExample {
    public static void main(String[] args) {
        double res = division(1, 0);
    }

    static double division(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException();
        }

        return a / b;
    }
}
