package ru.maksaimer.s02;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    byte a = 1;
	    byte b = 2;
	    byte c = (byte) (a + b);

	    int req = requestNumber();
        System.out.println(req);

	    inc();
	    aByte();
	    arrayPrint();
        printOdd();
    }

    public static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ведите число: ");

        return scanner.nextInt();
    }

    public static void inc() {
        byte a = 1;
        System.out.println(a++);
        System.out.println(++a);
    }

    public static void aByte() {
        byte a = (byte) 200;
        System.out.println(a);
    }

    public static void literal(long p) {
        Long z = 1L;
        literal(100);
    }

    public static void arrayPrint() {
        byte a[] = {1, 2, 3, 4};

        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }

        for (byte b : a) {
            System.out.println(b);
        }
    }

    public static void printOdd() {
        byte a[] = {1, 2, 3, 4};

        for (int i = 0; i < a.length; i++) {
            if (i % 2 != 0) {
                continue;
            }

            System.out.println(a[i]);
        }
    }
}
