package ru.maksaimer.s05;


public class SuperHero extends Hero implements Cloneable {
    private final int coef;

    public SuperHero(int power, int coef) {
        super(power);

        this.coef = coef;
    }

    @Override
    public SuperHero clone() {
        SuperHero newObject = new SuperHero(this.getPower(), coef);
        newObject.health = this.health;

        return newObject;
    }

    @Override
    public int hit(Hero hero) {
        int power = this.getPower() * this.coef;
        hero.health -= power;

        return power;
    }

    @Override
    public boolean equals(Object obj) {
        SuperHero i = (SuperHero) obj;

        return i.coef == this.coef
                && i.getPower() == this.getPower()
                && i.getSpeed() == this.getSpeed();
    }

    @Override
    public String toString() {
        return super.toString() +
                "Coef \t" + coef;
    }
}
