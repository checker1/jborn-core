package ru.maksaimer.s05;

public class Main {

    public static void main(String[] args) {
	    Hero rox = new Hero();
	    SuperHero nyx = new SuperHero(30, 2);

        System.out.println(rox);

	    nyx.hit(rox);

        System.out.println(rox);

        SuperHero newNyx = nyx.clone();

        System.out.println(nyx);
        System.out.println(newNyx);
        System.out.println(newNyx == nyx); // ссылки не равны

        System.out.println(rox instanceof SuperHero);
        System.out.println(rox instanceof Hero);
    }
}
