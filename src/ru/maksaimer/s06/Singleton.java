package ru.maksaimer.s06;

public class Singleton implements Cloneable {
    private static Singleton singleton;

    private Integer i;
    private int j;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }

        return singleton;
    }
}
