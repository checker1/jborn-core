package ru.maksaimer.s06;

public class Main {

    public static void main(String[] args) {
        singleton();

        System.out.println(groupHit(
                new Hero(),
                new Tank(),
                new BlasterTank(),
                new SuperHero(10, 2)
        ));
    }

    public static int groupHit(Hero hero, Hittable... hittable) {
        int totalDamage = 0;
        for (Hittable hater : hittable) {
            totalDamage += hater.hit(hero);
        }

        return totalDamage;
    }

    public static void singleton() {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        System.out.println(s1 == s2); // is equals
    }
}
