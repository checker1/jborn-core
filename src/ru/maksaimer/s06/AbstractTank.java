package ru.maksaimer.s06;

public abstract class AbstractTank implements Hittable {
    protected int health;
    protected int armor;
    protected int forwardSpeed;
    protected int reverseSpeed;
    protected int reloadTime;

    public abstract void takeAmmo(AbstractTank tank, int ammoPoints);
}
