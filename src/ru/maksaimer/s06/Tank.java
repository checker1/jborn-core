package ru.maksaimer.s06;

public class Tank extends AbstractTank {
    private int accuracy; // от 0 до 100
    private int damage;
    private int ammo;

    @Override
    public int hit(Hero enemy) {
        if (ammo != 0) {
            int damageProbability = Math.random() * 100 <= accuracy ? 1 : 0;
            enemy.health -= damage * damageProbability;
            ammo--;
        }
        return enemy.health;
    }

    @Override
    public void takeAmmo(AbstractTank tank, int ammoPoints) {

    }
}
