package ru.maksaimer.s06;

public interface Hittable {
    int hit(Hero hero);
}
