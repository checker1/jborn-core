package ru.maksaimer.s13.examples;

public class OverrideExample {
    public static void main(String[] args) {
        Sample sample = new ExtSample();
        sample.someMethod();
    }

    static class Sample {
        public void someMethod() {
            System.out.println("Sample");
        }
    }

    static class ExtSample extends Sample {
        @Override
        public void someMethod() {
            System.out.println("ExtSample");
        }
    }
}
