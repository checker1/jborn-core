package ru.maksaimer.s13.examples;

public class DeprecatedExample {
    public static void main(String[] args) {
        Sample sample = new Sample();
        sample.someMethod();
    }

    static class Sample {
        @Deprecated
        public void someMethod() {
            System.out.println("Sample");
        }
    }
}
