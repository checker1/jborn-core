package ru.maksaimer.s13.examples;

public class SuppressWarningsExample {
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        Sample sample = new Sample();
        sample.someMethod();
    }

    static class Sample {
        @Deprecated
        public void someMethod() {
            System.out.println("Sample");
        }
    }
}
