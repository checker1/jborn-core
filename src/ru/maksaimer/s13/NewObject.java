package ru.maksaimer.s13;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Documentation(
        authors = { "maksaimer", "ivanov" },
        description = "Класс для примера использования аннотаций",
        shortDescription = "Пример"
)
public class NewObject {
    private String firstName;
    private String secondName;

    public NewObject() {
    }

    public NewObject(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return "NewObject{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException {
        Class<?> aClass = Class.forName("ru.maksaimer.s13.NewObject");

        Constructor<?> constructor = aClass.getConstructor(String.class, String.class);
        NewObject newObject = (NewObject) constructor.newInstance("Mikhail", "Maksaimer");

        Method toString = aClass.getMethod("toString");
        String string = (String) toString.invoke(newObject);

        Documentation a = aClass.getAnnotation(Documentation.class);
        if (a != null) {
            System.out.println(Arrays.toString(a.authors()));
            System.out.println(a.description());
            System.out.println(a.shortDescription());
            System.out.println(a.since());
        }

        System.out.println(string);
    }
}
