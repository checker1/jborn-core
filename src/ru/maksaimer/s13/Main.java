package ru.maksaimer.s13;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    private static String INIT_VALUE = "INIT_VALUE";

    private final String value;

    public Main(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static void main(String[] args) throws ClassNotFoundException {
        Object main = new Main("Some Value");
        Class clazz = main.getClass();

        Class classFromClass = Main.class;
        Class classFromReference = Class.forName("ru.maksaimer.s13.Main");

        System.out.println("Является ли аннотацией: "           + clazz.isAnnotation());
        System.out.println("Является ли интерфейсом: "          + clazz.isInterface());
        System.out.println("Является ли массивом: "             + clazz.isArray());
        System.out.println("Является ли оберткой примитивов: "  + clazz.isPrimitive());
        System.out.println("Является ли перечислением: "        + clazz.isEnum());

        System.out.println();
        System.out.println("Конструкторы: ");
        for (Constructor constructor : clazz.getConstructors()) {
            System.out.println(constructor);
        }

        System.out.println();
        System.out.println("Поля: ");
        for (Field field : clazz.getDeclaredFields()) {
            System.out.println(field);
        }

        System.out.println();
        System.out.println("Методы: ");
        for (Method method : clazz.getMethods()) {
            System.out.println(method);
        }
    }
}
