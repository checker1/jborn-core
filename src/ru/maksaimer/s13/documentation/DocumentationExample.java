package ru.maksaimer.s13.documentation;

import ru.maksaimer.s13.Documentation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

@Documentation(
        authors = { "maksaimer", "ivanov" },
        description = "Класс для примера использования аннотаций",
        shortDescription = "Пример"
)
public class DocumentationExample {
    @Documentation(
            authors = "sidorov",
            description = "Полное описание",
            shortDescription = "Короткое описание",
            since = "1.8"
    )
    public void method() {

    }

    public static void main(String[] args) {
        DocumentationExample example = new DocumentationExample();
        Class<? extends DocumentationExample> aClass = example.getClass();

        // распечатываем все аннотации
        for (Annotation annotation : aClass.getAnnotations()) {
            System.out.println(annotation);
        }

        // но можно получить аннотацию необходимого нам класса
        Documentation a = aClass.getAnnotation(Documentation.class);
        if (a != null) {
            System.out.println(Arrays.toString(a.authors()));
            System.out.println(a.description());
            System.out.println(a.shortDescription());
            System.out.println(a.since());
        } else {
            // в противном случае данная аннотация не найдена
        }

        // итерируемся по всем методом класса и смотрим есть ли аннотация
        for (Method method : aClass.getMethods()) {
            Documentation b = method.getAnnotation(Documentation.class);
            if (b != null ) {
                System.out.println(Arrays.toString(b.authors()));
                System.out.println(b.description());
                System.out.println(b.shortDescription());
                System.out.println(b.since());
            } else {
                // в противном случае данная аннотация не найдена
            }
        }

    }
}
