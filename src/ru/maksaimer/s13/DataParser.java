package ru.maksaimer.s13;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DataParser {
    private final static String GLOBAL_DELIMITER    = "===================================================";
    private final static String DELIMITER           = "=";
    private final static String FIRST_NAME_FIELD    = "first_name";
    private final static String LAST_NAME_FIELD     = "last_name";
    private final static String AGE_FIELD           = "age";
    private final static String CITY_FIELD          = "city";

    public static void main(String[] args) throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String path = "/home/maksaimer/jborn/JBorn_Core_07_Maksaimer/src/ru/maksaimer/s13/data.in";
        // List<User> users = parse(path);
        List<User> users = parse(path, User.class);

        for (User user : users) {
            System.out.println(user);
        }
    }

    public static <T> List<T> parse(String path, Class<T> aClass) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<T> result = new ArrayList<>();

        try (
                BufferedReader reader = new BufferedReader(new FileReader(path))
        ) {
            String line;
            T item = null;
            while ((line = reader.readLine()) != null) {
                if (line.equals(GLOBAL_DELIMITER)) {
                    if (item != null) {
                        result.add(item);
                    }

                    Constructor<T> constructor = aClass.getConstructor();
                    item = constructor.newInstance();
                } else {
                    String[] split = line.split(DELIMITER);
                    String fieldName = split[0];
                    String value = split[1];

                    for (Field field : aClass.getDeclaredFields()) {
                        CustomField annotation = field.getAnnotation(CustomField.class);
                        if (annotation != null && fieldName.equals(annotation.name())) {
                            String name = field.getName();
                            String methodName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
                            Method method = aClass.getMethod(methodName, field.getType());

                            if (field.getType().equals(Integer.class)) {
                                method.invoke(item, Integer.valueOf(value));
                            } else {
                                method.invoke(item, value);
                            }

                            break;
                        }
                    }

                }
            }
        }

        return result;
    }

    public static List<User> parse(String path) throws IOException {
        List<User> users = new ArrayList<>();

        try (
            BufferedReader reader = new BufferedReader(new FileReader(path))
        ) {
            String line;
            User user = null;
            while ((line = reader.readLine()) != null) {
                if (line.equals(GLOBAL_DELIMITER)) {
                    if (user != null) {
                        users.add(user);
                    }

                    user = new User();
                } else {
                    String[] split = line.split(DELIMITER);
                    if (FIRST_NAME_FIELD.equals(split[0])) {
                        user.setFirstName(split[1]);
                    } else if (LAST_NAME_FIELD.equals(split[0])) {
                        user.setLastName(split[1]);
                    } else if (AGE_FIELD.equals(split[0])) {
                        user.setAge(Integer.valueOf(split[1]));
                    } else if (CITY_FIELD.equals(split[0])) {
                        user.setCity(split[1]);
                    }
                }
            }
        }

        return users;
    }

    @Target({ ElementType.FIELD })
    @Retention(RetentionPolicy.RUNTIME)
    @interface CustomField {
        String name();
    }

    static class User {
        @CustomField(name = "first_name")
        private String firstName;

        @CustomField(name = "last_name")
        private String lastName;

        @CustomField(name = "age")
        private Integer age;

        @CustomField(name = "city")
        private String city;

        public User() {
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "User{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age=" + age +
                    ", city='" + city + '\'' +
                    '}';
        }
    }
}
