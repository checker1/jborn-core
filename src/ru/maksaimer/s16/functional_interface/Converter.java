package ru.maksaimer.s16.functional_interface;

@FunctionalInterface
public interface Converter<T, S> {
    T convert(S source);
}
