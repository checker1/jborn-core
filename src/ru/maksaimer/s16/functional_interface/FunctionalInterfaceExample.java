package ru.maksaimer.s16.functional_interface;

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        Converter<String, Integer> converter = (i) -> String.format("Число %d", i);
        System.out.println(converter.convert(10));
    }
}
