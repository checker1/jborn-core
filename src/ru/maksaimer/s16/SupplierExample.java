package ru.maksaimer.s16;

import java.util.function.Supplier;

public class SupplierExample {
    static class Person {

    }

    public static void main(String[] args) {
        Supplier<Person> personSupplier = Person::new;
        Person person = personSupplier.get();   // new Person
    }
}
