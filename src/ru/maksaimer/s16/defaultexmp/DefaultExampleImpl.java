package ru.maksaimer.s16.defaultexmp;

public class DefaultExampleImpl implements DefaultExample {
    @Override
    public int pow(int a, int b) {
        return (int) Math.pow(a, b);
    }
}
