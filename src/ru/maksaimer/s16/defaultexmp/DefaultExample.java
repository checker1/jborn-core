package ru.maksaimer.s16.defaultexmp;

public interface DefaultExample {
    int pow(int a, int b);

    default int sqr(int a) {
        return pow(a, 2);
    }
}
