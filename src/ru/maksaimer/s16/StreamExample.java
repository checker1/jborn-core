package ru.maksaimer.s16;

import java.util.*;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class StreamExample {
    static class PersonOne {
        Integer id;
        String firstName;
        String lastName;

        public PersonOne(Integer id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }

    static class PersonTwo {
        String name;

        public PersonTwo(String name) {
            this.name = name;
        }
    }

    public static void main(String[] args) {
        List<PersonOne> persons = asList(
                new PersonOne(1, "Mikhail", "Maksaimer"),
                new PersonOne(2, "Ivan","Fedorov"),
                new PersonOne(3, "Fedor", "Ivanov"));

        List<PersonTwo> personTwoWithout = new ArrayList<>();
        for (PersonOne person : persons) {
            personTwoWithout.add(new PersonTwo(person.firstName + " " + person.lastName));
        }
        // vs
        List<PersonTwo> personsTwoStream = persons.stream()
                .map(p -> new PersonTwo(p.firstName + " " + p.lastName))
                .collect(toList());

        Map<Integer, PersonOne> mapWithout = new HashMap<>();
        for (PersonOne person : persons) {
            mapWithout.put(person.id, person);
        }
        // vs
        Map<Integer, PersonOne> mapStream = persons.stream().collect(toMap(p -> p.id, p -> p));

        List<PersonTwo> personThreeWithout = new ArrayList<>();
        for (PersonOne person : persons) {
            if (person.id == 1) {
                personThreeWithout.add(new PersonTwo(person.firstName + " " + person.lastName));
            }
        }
        // vs
        List<PersonTwo> personThreeStream = persons
                .stream()
                .filter(person -> person.id == 1)
                .map(p -> new PersonTwo(p.firstName + " " + p.lastName))
                .collect(toList());


        List<PersonTwo> personFourWithout = new ArrayList<>();
        persons.sort(Comparator.comparing(p -> p.firstName));
        for (PersonOne person : persons) {
            if (person.id == 1) {
                personThreeWithout.add(new PersonTwo(person.firstName + " " + person.lastName));
            }
        }
        // vs
        List<PersonTwo> personForuStream = persons
                .stream()
                .sorted(Comparator.comparing(p -> p.firstName))
                .filter(person -> person.id == 1)
                .map(p -> new PersonTwo(p.firstName + " " + p.lastName))
                .collect(toList());

        long count = 0;
        for (PersonOne person : persons) {
            if (person.id == 1) {
                count++;
            }
        }
        // vs
        count = persons
                .stream()
                .filter(person -> person.id == 1)
                .count();
    }
}
