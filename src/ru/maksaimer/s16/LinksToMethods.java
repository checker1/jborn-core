package ru.maksaimer.s16;

import ru.maksaimer.s16.functional_interface.Converter;

public class LinksToMethods {
    public static void main(String[] args) {
        Converter<String, Integer> converter = (i) -> i.toString();
        System.out.println(converter.convert(10));

        converter = Object::toString;
        System.out.println(converter.convert(10));
    }
}
