package ru.maksaimer.s16;

import java.util.List;

import static java.util.Arrays.asList;

public class LambdaExpression {
    public static void main(String[] args) {
        List<Integer> arr = asList(1, 32, 6, 10, -1, -12);
        arr.sort((o1, o2) -> Integer.compare(o1, o2));

        Thread t = new Thread(() -> System.out.println(Thread.currentThread().getName()));
        t.start();
    }
}
