package ru.maksaimer.s16;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toList;

public class ParallelStreamExample {
    public static void main(String[] args) {
        int max = 1000000;
        List<String> values = new ArrayList<>(max);
        for (int i = 0; i < max; i++) {
            UUID uuid = UUID.randomUUID();
            values.add(uuid.toString());
        }

        notParallel(values);
        parallel(values);
    }

    static List<String> notParallel(List<String> values) {
        long t0 = System.nanoTime();

        List<String> result = values.stream().sorted().collect(toList());

        long t1 = System.nanoTime();

        long millis = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.println(String.format("sequential sort took: %d ms", millis)); // 936 ms

        return result;
    }

    static List<String> parallel(List<String> values) {
        long t0 = System.nanoTime();

        List<String> result = values.parallelStream().sorted().collect(toList());

        long t1 = System.nanoTime();

        long millis = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.println(String.format("parallel sort took: %d ms", millis)); // 486 ms

        return result;
    }
}