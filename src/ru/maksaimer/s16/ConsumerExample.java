package ru.maksaimer.s16;

import java.util.function.Consumer;

public class ConsumerExample {
    static class Person {
        String firstName;
        String lastName;

        public Person(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }

    public static void main(String[] args) {
        Consumer<Person> greeter = (p) -> System.out.println("Hello, " + p.firstName);
        Consumer<Person> buyer = (p) -> System.out.println("Buy, " + p.firstName);

        greeter.andThen(buyer)
                .accept(new Person("Luke", "Skywalker"));
    }
}
