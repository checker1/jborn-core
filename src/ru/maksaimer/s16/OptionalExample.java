package ru.maksaimer.s16;

import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        Optional<String> optional = Optional.of("bam");

        if (optional.isPresent()) {
            someMethod(optional.get());
        } else {
            someMethod("fallback");
        }
        // vs
        someMethod(optional.orElse("fallback"));

        // выполнить если элемент есть
        optional.ifPresent((s) -> System.out.println(s.charAt(0)));
    }

    static void someMethod(String str) {
        // todo
    }
}
