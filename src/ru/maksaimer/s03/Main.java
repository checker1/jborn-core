package ru.maksaimer.s03;

public class Main {

    public static void main(String[] args) {
        enableStringPool();
        disableStringPool();
        unicode();
        concat();

        badPractice();
        goodPractice();

        cacheNumbers();
        nullPointerWithInteger();
    }

    private static void cacheNumbers() {
        // [-128, 127]

        Integer a = Integer.parseInt("123");
        Integer b = 123;

        System.out.println(a == b); // is true

        a = 128;
        b = 128;
        System.out.println(a == b); // is false
    }

    private static void nullPointerWithInteger() {
        Integer i = null;
        int a = i;
    }

    private static void goodPractice() {
        StringBuilder str = new StringBuilder("");

        for (int i = 0; i < 10; i ++) {
            str.append(" ").append(i);
        }

        System.out.println(str.toString());
    }

    private static void badPractice() {
        String str = "";

        for (int i = 0; i < 10; i ++) {
            str += " " + i;
        }

        System.out.println(str);
    }

    private static void unicode() {
        System.out.println("\u006a\u0061\u0076\u0061"); // java printed
    }

    private static void concat() {
        String a = "JBorn";
        String b = " - ";
        String c = "JBorn";

        System.out.println(a + b + c);
        System.out.println(a.concat(b).concat(c));
    }

    private static void equals() {
        String a = "JBorn";
        String b = "JBorn";

        System.out.println(a.equals(b)); // is true
    }

    private static void enableStringPool() {
        String a = "JBorn";
        String b = "JBorn";

        System.out.println(a == b); // is true
    }

    private static void disableStringPool() {
        String a = "JBorn";
        String b = new String("JBorn");

        System.out.println(a == b); // is false
    }

    private static void literal() {
        Long l = 200L; // L - required
        Float f = 200F; // F - required
        Double d = 200D; // D - required
    }
}
