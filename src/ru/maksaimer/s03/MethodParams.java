package ru.maksaimer.s03;

public class MethodParams {
    public static void main(String[] args) {
        int a = 1;
        String b = "First value";

        doSmth(a, b);

        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }

    private static void doSmth(int a, String b) {
        a = 2;
        b = "Second value";
    }
}
