package ru.maksaimer.s07.ships;

import ru.maksaimer.s07.Tank;

public class TankSpaceShip {
    private Tank[] units;

    public void load(Tank[] units) {
        this.units = units;
    }

    public Tank[] unload() {
        Tank[] units = this.units;
        this.units = null;

        return units;
    }

}
