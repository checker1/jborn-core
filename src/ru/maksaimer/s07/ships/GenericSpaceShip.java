package ru.maksaimer.s07.ships;

public class GenericSpaceShip<T> {
    private T[] units;

    public void load(T[] units) {
        this.units = units;
    }

    public T[] unload() {
        T[] units = this.units;
        this.units = null;

        return units;
    }
}
