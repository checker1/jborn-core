package ru.maksaimer.s07.ships;

import ru.maksaimer.s07.Transportable;

public class LimitedGenericSpaceShip<T extends Transportable> {
    private int size = 100;
    private T[] objects;

    public void load(T[] objects) {
        int sum = 0;
        for (T object : objects) {
            sum += object.getAmountOfSpace();
        }

        if (sum <= size) {
            this.objects = objects;
        }
    }

    public T[] unload() {
        T[] result = this.objects;

        this.objects = null;

        return result;
    }

    public T[] getObjects() {
        return objects;
    }
}
