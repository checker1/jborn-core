package ru.maksaimer.s07;

public interface Transportable {
    int getAmountOfSpace();
}
