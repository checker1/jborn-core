package ru.maksaimer.s07;

import ru.maksaimer.s07.ships.LimitedGenericSpaceShip;

public class Main {
    public static void main(String[] args) {
        LimitedGenericSpaceShip<Tank> spaceShip = new LimitedGenericSpaceShip<>();
        spaceShip.load(new Tank[]{
                new Tank(),
                new Tank(),
                new Tank(),
                new Tank(),
                new Tank()
        });

        Tank tank = find(new Tank(), spaceShip);
    }

    static int getAmount(LimitedGenericSpaceShip<? extends Transportable> spaceShip) {
        return spaceShip.getObjects().length;
    }

    static <T extends Transportable> T find(T unit, LimitedGenericSpaceShip<T> spaceShip) {
        for (Transportable o : spaceShip.getObjects()) {
            if (o.equals(unit)) return unit;
        }

        return null;
    }
}
