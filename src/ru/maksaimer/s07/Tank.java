package ru.maksaimer.s07;

public class Tank implements Transportable {
    @Override
    public int getAmountOfSpace() {
        return 2;
    }
}
