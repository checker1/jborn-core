package ru.maksaimer.s11;

import java.util.Arrays;

public class ComparableUser implements Comparable<ComparableUser> {
    String name;
    int age;

    public ComparableUser(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ",age=" + age +
                '}';
    }

    @Override
    public int compareTo(ComparableUser o) {
        return Integer.compare(this.age, o.age);
    }

    public static void main(String[] args) {
        ComparableUser[] users = {
                new ComparableUser("Михаил", 31),
                new ComparableUser("Александр", 23),
                new ComparableUser("Алексей", 19),
                new ComparableUser("Никита", 45),
                new ComparableUser("Саша", 39),
                new ComparableUser("Антон", 13),
        };

        System.out.println(Arrays.toString(users).replace(", ", "\n"));

        Arrays.sort(users);

        System.out.println("=============================================");
        System.out.println(Arrays.toString(users).replace(", ", "\n"));
    }
}
