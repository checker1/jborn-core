package ru.maksaimer.s11;

import java.util.Arrays;
import java.util.Comparator;

public class User {
    String name;
    int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ",age=" + age +
                '}';
    }    public static void main(String[] args) {
        User[] users = {
                new User("Михаил", 31),
                new User("Александр", 23),
                new User("Алексей", 19),
                new User("Никита", 45),
                new User("Саша", 39),
                new User("Антон", 13),
        };

        System.out.println(Arrays.toString(users).replace(", ", "\n"));

        Arrays.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return Integer.compare(o1.age, o2.age);
            }
        });

        System.out.println("=============================================");
        System.out.println(Arrays.toString(users).replace(", ", "\n"));
    }
}
