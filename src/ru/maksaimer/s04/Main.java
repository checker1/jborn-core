package ru.maksaimer.s04;

public class Main {

    public static void main(String[] args) {
        heroBattle();
	    dayOfTheWeek();
    }

    private static void dayOfTheWeek() {
        DayOfWeek dayOfWeek = DayOfWeek.SATURDAY;

        if (dayOfWeek.isDayOff()) {
            System.out.println("Day off"); // logic for day off days
        }

        System.out.println("Number day of the week: " + dayOfWeek.dayNumber());
    }

    private static void heroBattle() {
        Hero rox = new Hero(34);
        Hero nyx = new Hero();

        System.out.println(nyx.toString());

        rox.hit(nyx);

        System.out.println(nyx.toString());
    }
}
