package ru.maksaimer.s14_15.join_is_alive;

public class IsAliveExample implements Runnable {
    @Override
    public void run() {
        System.out.println("Start");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finish");
    }

    public static void main(String[] args) {
        IsAliveExample isAliveExample = new IsAliveExample();
        Thread thread = new Thread(isAliveExample);
        thread.start();

        while (thread.isAlive()) { /* ожидаем пока isAlive не станет false */ }

        System.out.println("Main Thread Finish");
    }
}
