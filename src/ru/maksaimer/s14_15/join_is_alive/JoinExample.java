package ru.maksaimer.s14_15.join_is_alive;

public class JoinExample implements Runnable {
    @Override
    public void run() {
        System.out.println("Start");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finish");
    }

    public static void main(String[] args) throws InterruptedException {
        JoinExample isAliveExample = new JoinExample();
        Thread thread = new Thread(isAliveExample);
        thread.start();

        thread.join();

        System.out.println("Main Thread Finish");
    }
}
