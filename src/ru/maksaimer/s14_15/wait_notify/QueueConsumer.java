package ru.maksaimer.s14_15.wait_notify;

import java.util.*;

import static java.lang.Thread.sleep;

public class QueueConsumer implements Runnable {
    private Queue<Integer> queue = new LinkedList<>();
    private Integer sum = 0;

    public synchronized void put(Integer i) {
        queue.add(i);

        System.out.println(i + " - New item to queue, wake up!");
        notifyAll();
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (this) {
                    Integer i = queue.poll();
                    if (i == null) {
                        System.out.println("Queue is empty, wait!");

                        wait();

                        System.out.println("Received notification about queue!");
                    } else {
                        sleep(200);
                        System.out.println(Thread.currentThread().getName() + " sum = " + sum + ", i = " + i);
                        sum += i;
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        QueueConsumer queueConsumer = new QueueConsumer();
        Thread thread = new Thread(queueConsumer);
        thread.start();

        sleep(3000);

        for (int i = 0; i < 10000; i++) {
            queueConsumer.put(i);
        }
    }
}
