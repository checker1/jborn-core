package ru.maksaimer.s14_15;

public class RunThread extends Thread {

    public static void main(String[] args) {
        Thread thread1 = new ExampleThread();
        thread1.start();

        Thread thread2 = new Thread(new ExampleRunnable());
        thread2.start();
    }

    private static class ExampleRunnable implements Runnable {
        @Override
        public void run() {

        }
    }

    private static class ExampleThread extends Thread {
        @Override
        public void run() {

        }
    }
}
