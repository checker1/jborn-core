package ru.maksaimer.s14_15.create_thread;

public class ThreadExample extends Thread {
    @Override
    @SuppressWarnings("Duplicates")
    public void run() {
        System.out.println(getName() + " start");

        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(getName() + " " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(getName() + " finish");
    }

    public static void main(String[] args) {
        Thread thread = new ThreadExample();
        thread.start();
    }
}
