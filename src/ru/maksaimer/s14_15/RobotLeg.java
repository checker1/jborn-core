package ru.maksaimer.s14_15;

public class RobotLeg implements Runnable {
    public RobotLeg(Object monitor, Object next, String name) {
        this.monitor = monitor;
        this.next = next;
        this.name = name;
    }

    private final String name;
    private final Object monitor;
    private final Object next;

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (monitor) {
                    monitor.wait();
                }

                System.out.println(name + "\t\t" + Thread.currentThread().getName());
                Thread.sleep(1000);

                synchronized (next) {
                    next.notify();
                }
            }
        } catch (InterruptedException ignored) {}
    }

    public static void main(String[] args) throws InterruptedException {
        Object m1 = new Object();
        Object m2 = new Object();

        new Thread(new RobotLeg(m1, m2, "First")).start();
        new Thread(new RobotLeg(m2, m1, "Second")).start();

        Thread.sleep(2000);

        synchronized (m1) {
            m1.notify();
        }
    }
}
